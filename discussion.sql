-- [Section] Advance selects
-- Exlude records or NOT operator

SELECT FROM songs WHERE id != 3; 

-- GREATER THAN OR EQUAL
SELECT * FROM songs WHERE id > 3;

SELECT * FROM songs WHERE id >= 3;

-- Less Than or Equal
SELECT * FROM songs WHERE id < 11;

SELECT * FROM songs WHERE id <= 11;

-- IN operator
SELECT * FROM songs WHERE id IN (1, 3, 11);

SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

-- Partial Matches.
SELECT * FROM songs WHERE song_name LIKE "%a"; -- Last Letter

SELECT * FROM songs WHERE song_name LIKE "a%"; -- First Letter

SELECT * FROM songs WHERE song_name LIKE "%cl%"; -- lahat ng position Letter

SELECT * FROM songs WHERE song_name LIKE "%s_y%";

SELECT * FROM songs WHERE song_name LIKE "%s%y%";

SELECT * FROM songs WHERE id LIKE "%1";

-- SORTING records

SELECT * FROM songs ORDER BY song_name ASC;

SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct record

SELECT DISTINCT genre FROM songs;

-- [INNER JOIN]
SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;

-- LEFT JOIN
SELECT albums.album_title, songs.song_name FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- JOINING Multiple tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artists_id
    JOIN songs ON albums.id = songs.album_id;

-- Right Join
SELECT * FROM albums RIGHT JOIN songs ON albums.id = songs.album_id;

-- OUTER JOIN
SELECT * FROM albums OUTER JOIN songs albums.id = songs.album_id UNION SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.album_id;